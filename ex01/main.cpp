//
// main.cpp for Temperature Conversion in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d06/ex01
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 13 15:37:50 2014 Jean Gravier
// Last update Mon Jan 13 18:35:08 2014 Jean Gravier
//

#include <iostream>
#include <string>
#include <iomanip>

int		main()
{
  float		temperature;
  std::string	scale;

  std::cin >> temperature >> scale;
  if (scale == "Celsius")
    {
      temperature = temperature * (9.0 / 5.0) + 32;
      std::cout << std::right << std::setw(16) <<  std::fixed << std::setprecision(3) << temperature;
      std::cout << std::right << std::setw(16) << "Fahrenheit" << std::endl;
    }
  else if (scale == "Fahrenheit")
    {
      temperature = (temperature - 32) * 5.0 / 9.0;
      std::cout << std::right << std::setw(16) << std::fixed << std::setprecision(3) << temperature;
      std::cout << std::right << std::setw(16) << "Celsius" << std::endl;
    }
  return 0;
}
