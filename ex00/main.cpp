//
// main.cpp for Piscine in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d06/ex00
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 13 12:36:15 2014 Jean Gravier
// Last update Mon Jan 13 19:11:34 2014 Jean Gravier
//

#include <iostream>
#include <string>
#include <fstream>

int		main(int argc, char **argv)
{
  std::ifstream file;
  char		chr;
  int		i;

  if (argc > 1)
    {
      for(i = 1; i < argc; i++)
	{
	  file.open(argv[i], std::ios::in);
	  if (file)
	    {
	      while (file.get(chr))
		std::cout << chr;
	      file.close();

	    }
	  else
	    {
	      std::cerr << "my_cat: <" << argv[i] << ">: No such file or directory" << std::endl;
	    }

	}
    }
  else
    std::cout << "my_cat: Usage : ./my_cat file [...]" << std::endl;

  return 0;
}
