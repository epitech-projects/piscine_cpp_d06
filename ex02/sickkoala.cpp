//
// sickkoala.cpp for Hopital in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d06/ex02
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Mon Jan 13 21:52:46 2014 Jean Gravier
// Last update Tue Jan 14 00:09:48 2014 Jean Gravier
//

#include "sickkoala.h"
#include <string>
#include <iostream>
#include <algorithm>

SickKoala::SickKoala(std::string koala_name)
{

  this->name = koala_name;
}

SickKoala::~SickKoala()
{
  std::cout << "Mr." << this->name << ": Kreooogg !! Je suis gueriiii !" << std::endl;
}

void	SickKoala::poke()
{
  std::cout << "Mr." << this->name <<": Gooeeeeerrk !! :'(" << std::endl;
}

bool		SickKoala::takeDrug(std::string drugName)
{
  std::string	lower_drugName = drugName;
  std::transform(lower_drugName.begin(), lower_drugName.end(), lower_drugName.begin(), ::tolower);

  if (lower_drugName == "mars")
    {
      std::cout << "Mr." << this->name << ": Mars, et ca Kreog !" << std::endl;
      return (true);
    }
  else if (drugName == "Buronzand")
    {
      std::cout << "Mr." << this->name << ": Et la fatigue a fait son temps !" << std::endl;
      return (true);
    }
  else
    {
      std::cout << "Mr." << this->name <<": Goerk !" << std::endl;
      return (false);
    }
}

void		SickKoala::overDrive(std::string str)
{
  size_t	current_pos;

  current_pos = 0;
  while ((current_pos = str.find("Kreog !")) != std::string::npos)
    str.replace(current_pos, 7, "1337 !");
  std::cout << "Mr." << this->name << ": " << str << std::endl;
}
