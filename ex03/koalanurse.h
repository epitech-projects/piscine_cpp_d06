/*
** koalanurse.h for nurse in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d06/ex03
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Tue Jan 14 00:15:31 2014 Jean Gravier
** Last update Tue Jan 14 01:15:49 2014 Jean Gravier
*/

#ifndef KOALANURSE_H_
#define KOALANURSE_H_

#include <string>
#include "sickkoala.h"

class		KoalaNurse
{
 public:
  KoalaNurse(int);
  ~KoalaNurse();
  void		giveDrug(std::string, SickKoala *);
  std::string	readReport(std::string);
  void		timeCheck();

 private:
  int	nurseId;
  bool	isWorking;
};

#endif /* !KOALANURSE_H_ */
