/*
** sickkoala.h for Hospital in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d06/ex02
**
** Made by Jean Gravier
** Login   <gravie_j@epitech.net>
**
** Started on  Mon Jan 13 19:01:10 2014 Jean Gravier
** Last update Mon Jan 13 23:13:23 2014 Jean Gravier
*/

#ifndef SICKKOALA_H_
#define SICKKOALA_H_

#include <string>

class		SickKoala
{
 public:
  SickKoala(std::string koala_name);
  void		poke();
  bool		takeDrug(std::string drugName);
  void		overDrive(std::string str);
  ~SickKoala();

 private:
  std::string	name;
};

#endif /* !SICKKOALA_H_ */
