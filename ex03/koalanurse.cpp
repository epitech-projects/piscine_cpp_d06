//
// koalanurse.cpp for Nurse in /home/gravie_j/Documents/projets/piscine/piscine_cpp_d06/ex03
//
// Made by Jean Gravier
// Login   <gravie_j@epitech.net>
//
// Started on  Tue Jan 14 00:20:09 2014 Jean Gravier
// Last update Tue Jan 14 01:45:45 2014 Jean Gravier
//

#include "koalanurse.h"
#include <string>
#include <iostream>
#include <fstream>

KoalaNurse::KoalaNurse(int id)
{
  this->nurseId = id;
  this->isWorking = false;
}

KoalaNurse::~KoalaNurse()
{
  std::cout << "Nurse " << this->nurseId << ": Enfin un peu de repos !" << std::endl;
}

void	KoalaNurse::giveDrug(std::string drug, SickKoala *patient)
{
  patient->takeDrug(drug);
}

std::string	KoalaNurse::readReport(std::string reportFile)
{
  std::ifstream	report(reportFile.c_str(), std::ios::in);
  std::string	drug, patient;
  size_t	first_point;

  if ((first_point = reportFile.find_last_of(".report")) != std::string::npos)
    patient = reportFile.substr(0, first_point - 6);
  if (report.good())
    {
      if (getline(report, drug))
	{
	  std::cout << "Nurse " << this->nurseId <<": Kreog ! Il faut donner un " << drug << " a Mr." << patient << " !" << std::endl;
	  return (drug);
	}
      report.close();
    }
  return ("");
}

void	KoalaNurse::timeCheck()
{
  if (this->isWorking)
    {
      std::cout << "Nurse " << this->nurseId << ": Je rentre dans ma foret d'eucalyptus !" << std::endl;
      this->isWorking = false;
    }
  else
    {
      std::cout << "Nurse " << this->nurseId << ": Je commence le travail !" << std::endl;
      this->isWorking = true;
    }
}
